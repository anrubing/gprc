<?php

declare(strict_types=1);

namespace Newcreate\Grpc\Client;

use Newcreate\Grpc\StatusCode;
use Newcreate\Grpc\Client\Exception\GrpcClientException;

class ClientStreamingCall extends StreamingCall
{
    /**
     * @var bool
     */
    private $received = false;

    public function recv(float $timeout = GrpcClient::GRPC_DEFAULT_TIMEOUT)
    {
        if (! $this->received) {
            $this->received = true;
            return parent::recv($timeout);
        }
        throw new GrpcClientException('ClientStreamingCall can only call recv once!', StatusCode::INTERNAL);
    }
}
