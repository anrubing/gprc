<?php

declare(strict_types=1);

namespace Newcreate\Grpc\Client;

use Google\Protobuf\Internal\Message;
use Newcreate\Grpc\Parser;
use Jean85\PrettyVersions;
use Swoole\Http2\Request as BaseRequest;

class Request extends BaseRequest
{
    private const DEFAULT_CONTENT_TYPE = 'application/grpc+proto';

    /**
     * @var null|bool
     */
    public $usePipelineRead;

    public function __construct(string $method, Message $argument = null, $headers = [])
    {
        $this->method = 'POST';
        $this->headers = array_replace($this->getDefaultHeaders(), $headers);
        $this->path = $method;
        $argument && $this->data = Parser::serializeMessage($argument);
    }

    public function getDefaultHeaders(): array
    {
        return [
            'content-type' => self::DEFAULT_CONTENT_TYPE,
            'te' => 'trailers',
            'user-agent' => $this->buildDefaultUserAgent(),
        ];
    }

    private function buildDefaultUserAgent(): string
    {
        $userAgent = 'grpc-php-easyswoole/1.0';
        $grpcClientVersion = PrettyVersions::getVersion('newcreate/grpc-client')->getPrettyVersion();
        if ($grpcClientVersion) {
            $explodedVersions = explode('@', $grpcClientVersion);
            $userAgent .= ' (easyswoole-grpc-client/' . $explodedVersions[0] . ')';
        }
        return $userAgent;
    }
}
