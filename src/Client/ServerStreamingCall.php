<?php


namespace Newcreate\Grpc\Client;

use Newcreate\Grpc\StatusCode;
use Newcreate\Grpc\Client\Exception\GrpcClientException;

/**
 * Represents an active call that sends a single message and then gets a
 * stream of responses.
 */
class ServerStreamingCall extends StreamingCall
{
    public function push($message): void
    {
        throw new GrpcClientException('ServerStreamingCall can not push data from client', StatusCode::INTERNAL);
    }
}
