<?php

declare(strict_types=1);

namespace Newcreate\Grpc\Client;

final class Status
{
    public const WAIT_PENDDING = 0;

    public const WAIT_FOR_ALL = 1;

    public const WAIT_CLOSE = 2;

    public const WAIT_CLOSE_FORCE = 3;
}
